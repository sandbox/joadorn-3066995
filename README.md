Medieval datation
-----------------

This module offers a field type with a double information (century, position in century).
The two lists of options are taxonomies created at install time.


Installation
------------

Please enable content translation and install french and english before installing the module.


Usage
------

Use the new field type "medieval datation" created during installation.