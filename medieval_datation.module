<?php
function medieval_datation_install()
{
    createTaxonomyFromYamlFile("century");
    createTaxonomyFromYamlFile("position_in_century");
}

function medieval_datation_uninstall()
{
    deleteTaxonomy("century");
    deleteTaxonomy("position_in_century");
}

/**
 * @param string $vocabulary_name
 */
function createTaxonomyFromYamlFile(string $vocabulary_name): void
{
    $centuries = \Symfony\Component\Yaml\Yaml::parseFile(drupal_get_path('module', 'medieval_datation') . "/vocabularies/{$vocabulary_name}.yml");
    $vid = createTaxonomy($centuries['vocabulary']);
    createTaxonomyTerms($centuries['vocabulary']['terms'], $vid);
}

/**
 * @param array $terms
 * @param $vid
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function createTaxonomyTerms(array $terms, $vid)
{
    $counter = 0;
    foreach ($terms as $value) {
        $counter++;
        $term = [
            'name' => $value['label']['fr'],
            'vid' => $vid,
            'langcode' => 'fr',
            'weight' => $counter
        ];
        $term = \Drupal\taxonomy\Entity\Term::create($term);
        $translated_fields = [];
        $translated_fields['name'] = $value['label']['en'];
        $term->addTranslation('en', $translated_fields);
        $term->save();
    }
}


/**
 * @param array $config
 * @return string $vid Vocabulary identifier
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function createTaxonomy(array $config)
{
    $vid = $config['vid'];
    $name = $config['title']['fr'];
    $vocabularies = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();
    if (!isset($vocabularies[$vid])) {
        $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::create(array(
            'vid' => $vid,
            'description' => '',
            'name' => $name,
        ));
        $vocabulary->save();
        return $vid;
    } else {
        throw new \Exception("Vocabulary {$vid} already exists");
    }

}

function deleteTaxonomy($vid)
{
    $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::load($vid);
    if ($vocabulary) {
        $vocabulary->delete();
    }

}