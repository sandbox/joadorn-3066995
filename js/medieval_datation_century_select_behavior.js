/**
 * @file
 */
(function () {
    function updateWidgetState($centuryMenu) {
        var $positionInCenturyMenu = $centuryMenu.closest("td").find('div[class$="position-in-century"]').first();
        if ($centuryMenu.val().match(/^$/)) {
            $positionInCenturyMenu.val(null).hide();
            $positionInCenturyMenu.find("select").val(null);
        } else {
            $positionInCenturyMenu.show();
        }
    }

    (function ($, Drupal) {
        Drupal.behaviors.conditionnaly_enable_positions_select = {
            attach: function (context, settings) {
                $('select[id$="-century"]', context).once('awesome').each(function (i, e) {
                    if (e.id.match(/-\d+-century/)) {
                        var $centuryMenu = $(e);
                        $centuryMenu.on("change", function (e) {
                            updateWidgetState($centuryMenu);
                        });
                        updateWidgetState($centuryMenu);
                    }
                });
            }
        };
    }(jQuery, Drupal));
}());