<?php

namespace Drupal\medieval_datation\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of Medieval Datation.
 *
 * @FieldType(
 *   id = "medieval_datation",
 *   label = @Translation("Medieval Datation"),
 *   default_formatter = "medieval_datation_formatter",
 *   default_widget = "medieval_datation_widget",
 *   category = @Translation("Custom"),
 * )
 */
class MedievalDatation extends \Drupal\Core\Field\FieldItemBase implements \Drupal\Core\Field\FieldItemInterface
{
    /**
     * {@inheritdoc}
     */
    public static function schema(\Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition)
    {
        return array(
            // columns contains the values that the field will store
            'columns' => array(
                // List the values that the field will save. This
                // field will only save a single value, 'value'
                'century' => array(
                    'type' => 'text',
                    'size' => 'tiny',
                    'not null' => FALSE,
                ),
                'position_in_century' => array(
                    'type' => 'text',
                    'size' => 'tiny',
                    'not null' => FALSE,
                ),
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(\Drupal\Core\Field\FieldStorageDefinitionInterface $storage)
    {

        $properties = [];

        $properties['century'] = DataDefinition::create('string')
            ->setLabel(t('Century'));

        $properties['position_in_century'] = DataDefinition::create('string')
            ->setLabel(t('Position in century'));

        return $properties;
    }


}
