<?php

namespace Drupal\medieval_datation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'FolioDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "medieval_datation_formatter",
 *   label = @Translation("Medieval datation"),
 *   field_types = {
 *     "medieval_datation"
 *   }
 * )
 */
class MedievalDatationFormatter extends FormatterBase
{

    /**
     * Define how the field type is showed.
     *
     * Inside this method we can customize how the field is displayed inside
     * pages.
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {

        $elements = [];
        foreach ($items as $delta => $item) {
            $centuryTerm = Drupal\taxonomy\Entity\Term::load($item->century);
            $centuryLabel = $centuryTerm ? $centuryTerm->getName() : "";
            $positionInCenturyTerm = $item->position_in_century ? Drupal\taxonomy\Entity\Term::load($item->position_in_century) : null;
            $positionInCenturyLabel = $positionInCenturyTerm ? $positionInCenturyTerm->getName() : "";
            $elements[$delta] = [
                '#type' => 'markup',
                '#markup' => $centuryLabel . ' ' . $positionInCenturyLabel
            ];
        }

        return $elements;
    }

} // class
