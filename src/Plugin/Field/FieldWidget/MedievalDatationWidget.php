<?php

namespace Drupal\medieval_datation\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'FolioDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "medieval_datation_widget",
 *   label = @Translation("Medieval datation"),
 *   field_types = {
 *     "medieval_datation"
 *   }
 * )
 */
class MedievalDatationWidget extends WidgetBase
{

    /**
     * Define the form for the field type.
     *
     * Inside this method we can define the form used to edit the field type.
     *
     * Here there is a list of allowed element types: https://goo.gl/XVd4tA
     */
    public function formElement(
        FieldItemListInterface $items,
        $delta,
        Array $element,
        Array &$form,
        FormStateInterface $formState
    )
    {
        $centuriesOptions = $this->createOptionsForVocabulary('century');
        $positionsInCenturyOptions = $this->createOptionsForVocabulary('position_in_century');
        $value = $items->getValue()[$delta];
        $element['century'] = [
            '#type' => 'select',
            '#empty_value' => NULL,
            '#empty_option' => $this
                ->t('Unknown'),
            '#title' => $this
                ->t('Century'),
            '#default_value' => isset($value) ? $value['century'] : NULL,
            '#options' =>
                $centuriesOptions,
        ];
        $element['position_in_century'] = [
            '#type' => 'select',
            '#empty_value' => NULL,
            '#empty_option' => $this
                ->t('Unknown'),
            '#title' => $this
                ->t('Position in century'),
            '#default_value' => isset($value) ? $value['position_in_century'] : NULL,
            '#options' =>
                $positionsInCenturyOptions,
        ];
        $element['#attached']['library'][] = 'medieval_datation/century';
        return $element;
    }

    /**
     * @param string $vid
     * @return mixed
     * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    private function getVocabulary(string $vid)
    {
        return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    }

    /**
     * @param string $vid
     * @return array
     * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    private function createOptionsForVocabulary(string $vid): array
    {
        $centuries = $this->getVocabulary($vid);
        $centuriesOptions = [];
        foreach ($centuries as $century) {
            $centuriesOptions[intval($century->tid)] = $century->name;
        }
        return $centuriesOptions;
    }

} // class
